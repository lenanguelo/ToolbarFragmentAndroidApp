package com.worldcompany.mytoolbar.Controllers.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.worldcompany.mytoolbar.Controllers.Fragments.MainFragment;
import com.worldcompany.mytoolbar.R;

public class MainFragmentActivity extends AppCompatActivity implements MainFragment.OnButtonClickedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);
    }

    // --------------
    // CallBack
    // --------------

    @Override
    public void onButtonClicked(View view) {
        Log.e(getClass().getSimpleName(),"Button clicked !");
        Intent detailActivityIntent = new Intent(this, DetailFragmentActivity.class);
        startActivity(detailActivityIntent);
    }
}