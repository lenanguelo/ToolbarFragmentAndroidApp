package com.worldcompany.mytoolbar.Controllers.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.worldcompany.mytoolbar.R;

public class DetailFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_fragment);
    }
}