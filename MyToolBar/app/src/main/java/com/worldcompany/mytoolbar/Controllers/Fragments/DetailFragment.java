package com.worldcompany.mytoolbar.Controllers.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.worldcompany.mytoolbar.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFragment extends Fragment {
    // 1 - Declare TextView
    private TextView textView;

    // 1 - Declare a buttonTag tracking
    private int buttonTag;
    // 2 - Create static variable to identify key in Bundle
    private static final String KEY_BUTTONTAG = "com.worldcompagny.mytoolbar.Controllers.Fragments.DetailFragment.KEY_BUTTONTAG";


    public DetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(String param1, String param2) {
        DetailFragment fragment = new DetailFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
             buttonTag = savedInstanceState.getInt(KEY_BUTTONTAG, 0);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        // 2 - Get textView from layout (don't forget to create ID in fragment_detail.xml)
        this.textView = (TextView) view.findViewById(R.id.fragment_detail_text_view);
        // 6 - Update TextView
        this.updateTextView(buttonTag);
        return (view);
    }

    // 3 - Update TextView depending on TAG's button
    public void updateTextView(int tag){
        // 3 - Save tag in ButtonTag variable
        this.buttonTag = tag;
        switch (tag){
            case 10:
                this.textView.setText("You're a very good programmer !");
                break;
            case 20:
                this.textView.setText("I do believe that Jon Snow is going to die in next season...");
                break;
            case 30:
                this.textView.setText("Maybe Game of Thrones next season will get back in 2040 ?");
                break;
            default:
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // 4 - Save buttonTag in Bundle when fragment is destroyed
        outState.putInt(KEY_BUTTONTAG, buttonTag);
    }


   /* @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // 5 - Restore last buttonTag if possible
        if (savedInstanceState != null) {
            int buttonTagRestored = savedInstanceState.getInt(KEY_BUTTONTAG, 0);
            // 6 - Update TextView
            this.updateTextView(buttonTagRestored);
        }
    }*/
}