package com.worldcompany.mytoolbar.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.worldcompany.mytoolbar.Controllers.Fragments.NewsPageFragment;
import com.worldcompany.mytoolbar.Controllers.Fragments.ParamPageFragment;
import com.worldcompany.mytoolbar.Controllers.Fragments.ProfilePageFragment;

public class PageAdaperDifferentFragment extends FragmentPagerAdapter {
    //Default Constructor
    public PageAdaperDifferentFragment(FragmentManager mgr) {
        super(mgr);
    }

    @Override
    public int getCount() {
        return(3);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: //Page number 1
                return NewsPageFragment.newInstance();
            case 1: //Page number 2
                return ProfilePageFragment.newInstance();
            case 2: //Page number 3
                return ParamPageFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: //Page number 1
                return "Fil d'actualité";
            case 1: //Page number 2
                return "Profil";
            case 2: //Page number 3
                return "Paramètre";
            default:
                return null;
        }
    }
}
