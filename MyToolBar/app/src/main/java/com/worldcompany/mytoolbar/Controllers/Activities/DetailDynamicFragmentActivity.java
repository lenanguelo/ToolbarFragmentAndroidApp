package com.worldcompany.mytoolbar.Controllers.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.worldcompany.mytoolbar.Controllers.Fragments.DetailFragment;
import com.worldcompany.mytoolbar.Controllers.Fragments.MainFragment;
import com.worldcompany.mytoolbar.R;

public class DetailDynamicFragmentActivity extends AppCompatActivity {

    // 1 - Create static variable to identify Intent
    public static final String EXTRA_BUTTON_TAG = "com.worldcompany.mytoolbar.Controllers.Activities.DetailDynamicFragmentActivity.EXTRA_BUTTON_TAG";
    DetailFragment mDetailFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_dynamic_fragment);
        this.configureAndShowDetailFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        // 3 - Call update method here because we are sure that DetailFragment is visible
        this.updateDetailFragmentTextViewWithIntentTag();
    }


    // --------------
    // UPDATE UI
    // --------------

    // 2 - Update DetailFragment with tag passed from Intent
    private void updateDetailFragmentTextViewWithIntentTag(){
        // Get button's tag from intent
        int buttonTag = getIntent().getIntExtra(EXTRA_BUTTON_TAG, 0);
        // Update DetailFragment's TextView
        mDetailFragment.updateTextView(buttonTag);
    }

    // --------------
    // FRAGMENTS
    // --------------

    private void configureAndShowDetailFragment(){
        // A - Get FragmentManager (Support) and Try to find existing instance of fragment in FrameLayout container
        mDetailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_detail);

        if (mDetailFragment == null) {
            // B - Create new detail fragment
            mDetailFragment = new DetailFragment();
            // C - Add it to FrameLayout container
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_layout_detail, mDetailFragment)
                    .commit();
        }
    }
}