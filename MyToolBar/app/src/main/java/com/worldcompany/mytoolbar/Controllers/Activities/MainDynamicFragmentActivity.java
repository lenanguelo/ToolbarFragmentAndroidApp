package com.worldcompany.mytoolbar.Controllers.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.worldcompany.mytoolbar.Controllers.Fragments.DetailFragment;
import com.worldcompany.mytoolbar.Controllers.Fragments.MainFragment;
import com.worldcompany.mytoolbar.R;

public class MainDynamicFragmentActivity extends AppCompatActivity implements MainFragment.OnButtonClickedListener{

    // 1 - Declare main fragment
    private MainFragment mainFragment;
    // 1 - Declare detail fragment
    private DetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(getClass().getSimpleName(),"***** enter into Main Dynamic Fragment **** !");
        setContentView(R.layout.activity_main_dynamic_fragment);

        // 2 - Configure and show home fragment
        this.configureAndShowMainFragment();
        // 2 - Configure and show detail fragment
        this.configureAndShowDetailFragment();
    }

    // --------------
    // FRAGMENTS
    // --------------

    private void configureAndShowMainFragment(){
        // A - Get FragmentManager (Support) and Try to find existing instance of fragment in FrameLayout container
        mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_main);

        if (mainFragment == null) {
            // B - Create new main fragment
            mainFragment = new MainFragment();
            // C - Add it to FrameLayout container
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_layout_main, mainFragment)
                    .commit();
        }
    }

    // --------------
    // CallBack
    // --------------

    @Override
    public void onButtonClicked(View view) {
        Log.e(getClass().getSimpleName(),"Button clicked !");
       /* // 3 - Check if detail fragment is not created or if not visible
        if (detailFragment == null || !detailFragment.isVisible()){
            Intent detailActivityIntent = new Intent(this, DetailFragmentActivity.class);
            startActivity(detailActivityIntent);
        }*/

        // 1 - Retrieve button tag
        int buttonTag = Integer.parseInt(view.getTag().toString());
        // 2 - Check if DetailFragment is visible (Tablet)
        if (detailFragment != null && detailFragment.isVisible()) {
            // 2.1 - TABLET : Update directly TextView
            detailFragment.updateTextView(buttonTag);
        } else {
            // 2.2 - SMARTPHONE : Pass tag to the new intent that will show DetailActivity (and so DetailFragment)
            Intent i = new Intent(this, DetailDynamicFragmentActivity.class);
            i.putExtra(DetailDynamicFragmentActivity.EXTRA_BUTTON_TAG, buttonTag);
            startActivity(i);
        }

    }

    private void configureAndShowDetailFragment(){
        detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_detail);

        //A - We only add DetailFragment in Tablet mode (If found frame_layout_detail)
        if (detailFragment == null && findViewById(R.id.frame_layout_detail) != null) {
            detailFragment = new DetailFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_layout_detail, detailFragment)
                    .commit();
        }
    }
}